

from distutils.command.build_py import build_py
from distutils.command.install  import install
from distutils.core import setup
import os

class GCP_Module_Build(build_py):
   pass
class GCP_Module_Install(install):
  def run( self ):
       content =open("../.gcp_user","r")
       home = "/"+content.read().strip()
       content.close()
       if not os.path.isdir(home+"/.gcp"):
	  os.makedirs(home+"/.gcp")
       if not os.path.isdir(home+"/.gcp/files"):
	  os.makedirs(home+"/.gcp/files")
       if not os.path.isdir(home+"/.gcp/scripts"):
	  os.makedirs(home+"/.gcp/scripts")
	 
       install.run( self )

     

setup( 
  name="GCP Communicator",
  description="Module to speak to GCP bridge",
  cmdclass=dict( install=GCP_Module_Install, build_py=GCP_Module_Build ),
  packages=['gcp']
)





