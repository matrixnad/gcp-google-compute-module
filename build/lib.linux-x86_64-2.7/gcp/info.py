

class GCP_Messages( object ):
   GCP_INSTANCE_CONFIGURING = "Instance is being configured"
   GCP_INSTANCE_RUNNING = "Instance is being run.."
   GCP_INSTANCE_STARTING = "Instance is starting.."
   GCP_INSTANCE_PROCESSING = "Instance is processing"
   GCP_INSTANCE_STOPPED = "Instance was stopped"

