
import requests
import json
import os
## get our instance information

def getConfig():
	 file = open("/usr/local/gcp/config")
	 content = file.read()
   	 return json.loads( content )
	 

class GCP_Client( object ):
   @staticmethod
   def getInstance(id):
	 endpoint = "http://146.148.57.24/"
	 information  =requests.get(
			endpoint+"/get_configuration"
		)
	 loaded = json.loads(information)
	 return loaded 
   @staticmethod 
   def getActiveJobs():
        endpoint = requests.get(
			endpoint+"/get_jobs"
		)
	loaded = json.loads( endpoint.content )
	return loaded
   @staticmethod
   def getInstanceJob(instanceId):
       endpoint = os.getenv("GCP_MASTER_ENDPOINT")
       content= requests.get(
		endpoint+"/api/instance/"+instanceId+"/job")
       return json.loads( content.content )
   @staticmethod
   def getInstanceInstance(instanceId):
       endpoint = os.getenv("GCP_MASTER_ENDPOINT")
       request = requests.get(
		endpoint+"/api/instance/"+instanceId
		)
       return json.loads( request.content)
   @staticmethod
   def postFileResult(fileId, data=""):
        endpoint= os.getenv("GCP_MASTER_ENDPOINT")
	resp = requests.post(
		endpoint+"/api/file/"+fileId,
		json=dict(
			job_file_result=data
			) )
	return json.loads( resp.content )
   @staticmethod
   def reportError( instanceId, fileId=None,data="" ):
	 return GCP_Client.report(instanceId=instanceId, fileId=fileId, typeOfReport="ERROR", data=data)
   @staticmethod
   def reportSuccess( instanceId, fileId=None, data=""):
	 return GCP_Client.report(instanceId=instanceId, fileId=fileId, typeOfReport="SUCCESS", data=data)
   @staticmethod
   def reportInfo( instanceId, fileId=None, data="" ): 
	 return GCP_Client.report(instanceId=instanceId, fileId=fileId, typeOfReport="INFO", data=data)

   @staticmethod
   def report( instanceId="", fileId=None, typeOfReport="", data=""):
  	 endpoint = os.getenv("GCP_MASTER_ENDPOINT")
         if fileId:
	    url = endpoint+"/api/instance/"+instanceId+"/file/"+fileId+"/report"
	 else:
	    url = endpoint+"/api/instance/"+instanceId+"/report"
	 resp = requests.post(
			url, 
			json= dict(
				job_report_type=typeOfReport,
				job_report_data=data
				)
		)
	 return json.loads(resp.content)
		
   
			
		
			



        



