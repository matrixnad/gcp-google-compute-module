#! /usr/bin/python2.7
import requests
import gcp
import os
from .client import GCP_Client
from .startup_master import GCPStartup





## this should run after our initial GCP_INSTANCE_ID export
class GCPMain( object ):
   @staticmethod
   def  runMain( ):
      endpoint = os.getenv("GCP_MASTER_ENDPOINT")
      instanceId =  os.getenv("GCP_INSTANCE_ID")
      instanceInfo  = GCP_Client.getInstanceInstance( str(instanceId) )
      instanceObject = GCPStartup.fromInstanceConfiguration( instanceInfo['data'][0] )
      instanceObject.configure()
      instanceObject.run()
  
