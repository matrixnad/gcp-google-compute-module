


import gcp
import os
from .client import GCP_Client
from .process_cmd import GCPProcessCmd
from .utils import decodeApiFile,getUser
from .info import GCP_Messages
#from .startup_debian import GCP_Startup_Debian
 
import uuid

class GCPStartup(object):
   def  __init__( self, configuration = dict() ):
	 self.configuration =configuration
	 self.files = []
	 self.script = ""
	 self.process_cmd = GCPProcessCmd()
   @staticmethod
   def fromInstanceConfiguration( configuration ):
       ##if configuration['configuration']['family']=="centos":
       ##	   return GCP_Startup_CentOS( configuration )
       return GCP_Startup_Debian( configuration )
   def configure( self ):
	 GCP_Client.reportInfo( str(self.configuration['id']), fileId="", data=GCP_Messages.GCP_INSTANCE_CONFIGURING)
	 pass
   def  run( self ):
	GCP_Client.reportInfo( str(self.configuration['id']), fileId="", data=GCP_Messages.GCP_INSTANCE_RUNNING)
 	job =GCP_Client.getInstanceJob( str(self.configuration['id']) )
	files = []
   	home =getUser()
	for i in job['data'][0]['job_files']:
		data = i
		data['job_file_data'] = decodeApiFile( i['job_file_data'] )
		new_file = home+"/.gcp/files/"+str(uuid.uuid4())
		opened = open(new_file, "w+")
		opened.write( data['job_file_data'] )
		opened.close()
		data['name']=new_file
		self.files.append(  new_file )

	scriptname = home+"/.gcp/scripts/"+str(uuid.uuid4())
	scripthandler= open(scriptname, "w+")
	#self.script = decodeApiFile( i['job_shell_script'])
	scripthandler.write( decodeApiFile(job['data'][0]['job_shell_script']) )
        scripthandler.close()
	self.process_cmd.start( str(self.configuration['id']), scriptname, self.files )
	


class GCP_Startup_Debian( GCPStartup ):
   pass


    

   
       
	    



